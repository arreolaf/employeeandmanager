/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package employee.and.manager;

/**
 *
 * @author mymac
 */
public class Manager extends Employee {
    private double bonus;
     
     
     Manager(){
          
     }
     
     Manager(String name, double hourlyWage, double hoursWorked, double bonus){
          super(name,hourlyWage, hoursWorked);
          this.bonus = bonus;
     }
     
     
     public double calculatePay() {
          return (super.calculatePay()) + bonus;

        }       
}
