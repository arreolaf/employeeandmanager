/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package employee.and.manager;

/**
 *
 * @author mymac
 */
public class Employee {
    
    private String name;
     private double hourlyWage;
     private double hoursWorked;
     
     Employee(){
          
     }
     
     Employee(String name, double hourlyWage, double hoursWorked){
          this.name = name;
          this.hourlyWage = hourlyWage;
          this.hoursWorked = hoursWorked;
     }
     
     public double calculatePay() {
          return hourlyWage * hoursWorked;
     }
 
     public String getName() {
          return name;
     }
 
     public double getHourlyWage() {
          return hourlyWage;
     }
 
     public double getHoursWorked() {
          return hoursWorked;
     }
}
 

    

